﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pakketboot.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T Get(int id);
        T Add(T dataObject);
        T Delete(T dataObject);
        int SaveContext();
    }
}
