﻿using Pakketboot.Domains.Enums;


namespace Pakketboot.Domains.Tests.Core
{
    public class DomainsFactory
    {
        public static User CreateUser(int index, SocialNetworkAccountType socialNetworkAccountType)
        {
            var user = new User() {
                UserId = index,
                Email = string.Format("testuser{0}@example.com", index),
                UserStatus = UserStatus.Active
            };
            var socialNetworkAccount = new SocialNetworkAccount() {
                SocialNetworkAccountId = index,
                Uid = index.ToString(),
                FirstName = string.Format("John{0}", index),
                LastName = string.Format("Doe{0}", index)
            };

            switch (socialNetworkAccountType) {
                case SocialNetworkAccountType.Local:
                    var localAccount = new LocalAccount(string.Format("password{0}", index)) {
                        SocialNetworkAccountId = socialNetworkAccount.SocialNetworkAccountId,
                        Uid = socialNetworkAccount.Uid,
                        SocialNetworkAccountType = socialNetworkAccount.SocialNetworkAccountType,
                        AttachDate = socialNetworkAccount.AttachDate,
                        FirstName = socialNetworkAccount.FirstName,
                        LastName = socialNetworkAccount.LastName
                    };
                    user.SocialNetworkAccounts.Add(localAccount);
                    return user;
                case SocialNetworkAccountType.Facebook:
                    socialNetworkAccount.SocialNetworkAccountType = SocialNetworkAccountType.Facebook;
                    user.SocialNetworkAccounts.Add(socialNetworkAccount);
                    return user;
                default:
                    return null;
            }
        }
    }
}
