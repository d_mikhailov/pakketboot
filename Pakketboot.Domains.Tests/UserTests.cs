﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Pakketboot.Domains.Enums;
using Pakketboot.Domains.Helpers;
using Pakketboot.Domains.Tests.Core;


namespace Pakketboot.Domains.Tests
{
    [TestFixture]
    public class UserTests
    {
        #region SETUP
        /*
        // TODO: Should be resolved
        private User user;
        private SocialNetworkAccount socialNetworkAccount;
        private Role role;
        

        [SetUp]
        public new void SetUp()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var socialNetworkAccount = new SocialNetworkAccount()
            {
                SocialNetworkAccountId = 12,
                Uid = "12",
                SocialNetworkAccountType = SocialNetworkAccountType.Facebook,
                FirstName = "John",
                LastName = "Doe"
            };
        }*/
        #endregion
        
        [Test]
        public void AddRole_AddNewRoleforUser_SuccessfullyAddingNewRoleReturnTrue()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var role = new Role() { RoleId = 1, Name = "user" };

            var result = user.AddRole(role);
            Assert.IsTrue(user.Roles.Any(r => r.RoleId.Equals(role.RoleId)));
            Assert.IsTrue(result);
        }

        [Test]
        public void AddRole_AddAlreadyRole_ExistingRoleNotAddedThrowException()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var role = new Role() { RoleId = 1, Name = "user" };
            user.Roles.Add(role);

            Assert.Throws<Exception>(() => user.AddRole(role));
        }

        [Test]
        public void AddRole_AddAlreadyInRoleWithUpperCase_ExistingRoleNotAddedThrowException()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var role = new Role() { RoleId = 1, Name = "user" };
            user.Roles.Add(role);

            Assert.Throws<Exception>(() => user.AddRole(new Role() { RoleId = 1, Name = "UsEr" }));
        }

        [Test]
        public void RemoveRole_RemoveExistingRole_ExistingRoleSuccessfullyRemovedReturnTrue()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var role = new Role() { RoleId = 1, Name = "user" };
            user.Roles.Add(role);

            var result = user.RemoveRole(role);

            Assert.IsTrue(result);
            Assert.IsFalse(user.Roles.Contains(role));
            Assert.IsNull(user.Roles.SingleOrDefault(r => r.RoleId.Equals(role.RoleId)));
        }

        [Test]
        public void RemoveRole_RemoveRoleWhenUserNotInRole_NothingForRemoveThrowException()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var role = new Role() { RoleId = 1, Name = "user" };

            Assert.Throws<Exception>(() => user.RemoveRole(role));
            Assert.IsFalse(user.Roles.Contains(role));
            Assert.IsNull(user.Roles.SingleOrDefault(r => r.RoleId.Equals(role.RoleId)));
        }

        [Test]
        public void AddSocialNetworkAccount_AddNewSocialNetworkAccount_AccountSuccessfullyAddedReturnTrue()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var socialNetworkAccount = new SocialNetworkAccount()
            {
                SocialNetworkAccountId = 12,
                Uid = "12",
                SocialNetworkAccountType = SocialNetworkAccountType.Facebook,
                FirstName = "John",
                LastName = "Doe"
            };

            var result = user.AddSocialNetworkAccount(socialNetworkAccount);

            Assert.IsTrue(result);
            Assert.IsTrue(user.SocialNetworkAccounts.Contains(socialNetworkAccount));
            Assert.AreEqual(1,
                user.SocialNetworkAccounts.Count(
                    s => s.SocialNetworkAccountType.Equals(SocialNetworkAccountType.Facebook)));
        }

        [Test]
        public void AddSocialNetworkAccount_AddAlreadyAddedNetworkAccountWithSameType_NothingForAddingThrowException()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var socialNetworkAccount = new SocialNetworkAccount()
            {
                SocialNetworkAccountId = 12,
                Uid = "12",
                SocialNetworkAccountType = SocialNetworkAccountType.Facebook,
                FirstName = "John",
                LastName = "Doe"
            };
            user.SocialNetworkAccounts.Add(socialNetworkAccount);

            Assert.Throws<Exception>(() => user.AddSocialNetworkAccount(socialNetworkAccount));
        }

        [Test]
        public void LocalAccountWithHashPassword_CreatingNewLocalAccountWithHashingPassword()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            var localAccount = user.SocialNetworkAccounts.Single(s => s.SocialNetworkAccountType.Equals(SocialNetworkAccountType.Local)) as LocalAccount;
            Assert.AreNotEqual("password1", localAccount.Password);
        }

        [Test]
        public void SetPassword_SettingPasswordForLocalAccountAsHash()
        {
            var user = DomainsFactory.CreateUser(1, SocialNetworkAccountType.Local);
            const string NEW_PASSWORD = "newpassword";

            var localAccount = user.SocialNetworkAccounts.Single(s => s.SocialNetworkAccountType.Equals(SocialNetworkAccountType.Local)) as LocalAccount;
            localAccount.SetPassword(NEW_PASSWORD);
            Assert.AreEqual(AccountHelpers.GetHash(NEW_PASSWORD), localAccount.Password);
        }

    }
}
