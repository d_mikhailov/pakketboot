﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pakketboot.Domains.Enums;


namespace Pakketboot.Domains
{
    public class SocialNetworkAccount
    {
        public SocialNetworkAccount()
        {
            AttachDate = DateTimeOffset.Now;
        }
        public int SocialNetworkAccountId { get; set; }
        public string Uid { get; set; }
        public SocialNetworkAccountType SocialNetworkAccountType { get; set; }
        public DateTimeOffset AttachDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
