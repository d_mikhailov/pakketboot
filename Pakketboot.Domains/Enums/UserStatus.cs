﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pakketboot.Domains.Enums
{
    public enum UserStatus
    {
        Active = 0,
        Blocked = 1
    }
}
