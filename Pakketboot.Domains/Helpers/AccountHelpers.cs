﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pakketboot.Domains.Helpers
{
    public class AccountHelpers
    {
        /// <summary>
        /// Creating SHA256 hash from string.
        /// </summary>
        /// <param name="pureString">String for hashing.</param>
        /// <returns>Return SHA256 string hash.</returns>
        public static string GetHash(string pureString)
        {
            var sha256 = new SHA256Managed();
            var bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pureString), 0, Encoding.UTF8.GetByteCount(pureString));
            var hash = string.Empty;
            hash = bytes.Aggregate(hash, (current, bit) => current + bit.ToString("x2"));
            return hash.ToUpper();
        }
    }
}
