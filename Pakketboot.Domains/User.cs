﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pakketboot.Domains.Enums;


namespace Pakketboot.Domains
{
    public class User
    {
        public User()
        {
            CreationDate = DateTimeOffset.Now;
            SocialNetworkAccounts = new List<SocialNetworkAccount>();
            Roles = new List<Role>();
        }
        public int UserId { get; set; }
        public string Email { get; set; }
        public UserStatus UserStatus { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public virtual ICollection<SocialNetworkAccount> SocialNetworkAccounts { get; set; } 
        public virtual ICollection<Role> Roles { get; set; }

        public bool AddSocialNetworkAccount(SocialNetworkAccount socialNetworkAccount)
        {
            var isAccountAlreadyAttached = (SocialNetworkAccounts.Any(s => s.Uid.Equals(socialNetworkAccount.Uid, StringComparison.OrdinalIgnoreCase)
                && s.SocialNetworkAccountType.Equals(socialNetworkAccount.SocialNetworkAccountType))
                                            || SocialNetworkAccounts.Any(s => s.SocialNetworkAccountType.Equals(socialNetworkAccount.SocialNetworkAccountType)));
            if (isAccountAlreadyAttached) {
                throw new Exception(string.Format("User {0}: User already attach social network account with id: {1} and type {2}", UserId, socialNetworkAccount.SocialNetworkAccountId, socialNetworkAccount.SocialNetworkAccountType));
            }
            SocialNetworkAccounts.Add(socialNetworkAccount);
            return true;
        }

        public bool AddRole(Role role)
        {
            var isUserInRole = Roles.Any(r => r.Name.Equals(role.Name, StringComparison.OrdinalIgnoreCase));
            if (isUserInRole) {
                throw new Exception(string.Format("User {0}: User already in role: {1}", UserId, role.Name ));
            }
            Roles.Add(role);
            return true;
        }

        public bool RemoveRole(Role role)
        {
            var isUserInRole = Roles.Any(r => r.Name.Equals(role.Name, StringComparison.OrdinalIgnoreCase));
            if (!isUserInRole) {
                throw new Exception(string.Format("User {0}: User not in role: {1}", UserId, role.Name));
            }
            Roles.Remove(role);
            return true;
        }
    }
}
