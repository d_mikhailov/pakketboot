﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pakketboot.Domains.Helpers;


namespace Pakketboot.Domains
{
    public class LocalAccount : SocialNetworkAccount
    {
        public LocalAccount(string purePassword)
        {
            Password = AccountHelpers.GetHash(purePassword);
        }

        public string Password { get; private set; }

        public void SetPassword(string purePassword)
        {
            Password = AccountHelpers.GetHash(purePassword);
        }
    }
}
