﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace Pakketboot.Web.Api
{
    public class OAuthConfig
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerAuthenticationOptions { get; set; }
        public static void Register(HttpConfiguration config, IAppBuilder app)
        {
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerAuthenticationOptions = new OAuthBearerAuthenticationOptions();
            var authServerOptions = new OAuthAuthorizationServerOptions() {
                AllowInsecureHttp = true,
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
            };

            app.UseOAuthAuthorizationServer(authServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerAuthenticationOptions);
        }
    }
}