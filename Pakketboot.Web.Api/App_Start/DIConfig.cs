﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Pakketboot.Domains;
using Pakketboot.Repositories.EntityFramework;
using Pakketboot.Repositories.Interfaces;

namespace Pakketboot.Web.Api
{
    public class DIConfig
    {
        public static void RegisterDI(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            // Register WebAPI controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // Init filter injections
            builder.RegisterWebApiFilterProvider(configuration);
            // Database settings
            builder.RegisterType<Context>()
                .As<DbContext>()
                .WithParameter("connectionString", "Pakketboot")
                .InstancePerRequest();

            // Add repositories
            builder.RegisterType<BaseRepository<User>>()
                .As<IBaseRepository<User>>();
            builder.RegisterType<BaseRepository<Role>>()
                .As<IBaseRepository<Role>>();

            // Builder container
            var container = builder.Build();

            // Create dependency resolver
            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure WebAPI dependency resolver
            configuration.DependencyResolver = resolver;
        }
    }
}