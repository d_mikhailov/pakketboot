﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Pakketboot.Web.Api.Core.Startup))]

namespace Pakketboot.Web.Api.Core
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            if (Boolean.Parse(ConfigurationManager.AppSettings["IsProfileEnabled"])) {
                HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
            }
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);
            //FilterConfig.RegisterMvcFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DIConfig.RegisterDI(GlobalConfiguration.Configuration);
            //MapperConfig.RegisterMappings();
            //MapperConfig.RegisterExternalMappings();
            //DatabaseConfig.RegisterDatabase();
            //PreStartConfig.InitializeAsync();
            
            OAuthConfig.Register(GlobalConfiguration.Configuration, app);
        }
    }
}
