﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pakketboot.Domains;

namespace Pakketboot.Repositories.EntityFramework
{
    public class Context : DbContext
    {
        public Context() : base("Pakketboot") { }
        public Context(string connectionString) : base(connectionString) { }

        public DbSet<User> Users { get; set; }
        public DbSet<SocialNetworkAccount> SocialNetworkAccounts { get; set; }
        public DbSet<LocalAccount> LocalAccounts { get; set; }
        public DbSet<Role> Roles { get; set; } 
    }
}
