﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pakketboot.Repositories.Interfaces;

namespace Pakketboot.Repositories.EntityFramework
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected DbContext _context;

        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public T Get(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public T Add(T dataObject)
        {
            return _context.Set<T>().Add(dataObject);
        }

        public T Delete(T dataObject)
        {
            return _context.Set<T>().Remove(dataObject);
        }

        public int SaveContext()
        {
            return _context.SaveChanges();
        }
    }
}
